import "@testing-library/jest-dom/extend-expect";
import { render, screen } from "@testing-library/react";
import React from "react";
import Display from "./Display";

describe("Display", () => {
  test("it should handle required", () => {
    render(<Display value={10} title={"My title"} required={true} />);

    const display = screen.getByTestId("Display");

    expect(display).toBeInTheDocument();
  });

  test("it should handle low value", () => {
    render(<Display value={3} title={"My title"} required={false} />);

    const display = screen.getByTestId("Display");

    expect(display).toBeInTheDocument();
  });

  test("it should handle big value", () => {
    render(<Display value={23} title={"My title"} required={false} />);

    const display = screen.getByTestId("Display");

    expect(display).toBeInTheDocument();
  });
});
